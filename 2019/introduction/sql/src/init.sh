#!/bin/sh
sed -i "s/#FLAG#/$FLAG/g" /var/www/localhost/htdocs/index.php
unset FLAG

# Start mysqld_safe in background, and wait for it to start
/usr/bin/mysqld_safe &
sleep 5

# set password for mysqld (randomly generated)
PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 100)
/usr/bin/mysqladmin -u root password $PASS

# insert our data
mysql -uroot -p${PASS} < /setup.sql

# start php7-fpm in background
/usr/sbin/php-fpm7 &

# start nginx
/usr/sbin/nginx &

while sleep 10; do
    ps aux |grep nginx |grep -q -v grep
    NGINX_STATUS=$?
    ps aux |grep mysqld_safe |grep -q -v grep
    MYSQLD_STATUS=$?
    ps aux |grep php-fpm7 |grep -q -v grep
    PHP_STATUS=$?

    # If the greps above find anything, they exit with 0 status
    # If they are not both 0, then something is wrong
    if [ $NGINX_STATUS -ne 0 -o $MYSQLD_STATUS -ne 0 -o $PHP_STATUS -ne 0 ]; then
      echo "Nginx, mysqld or php has exited."
      exit 1
    fi
done
