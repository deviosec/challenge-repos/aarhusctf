#!/bin/sh

sed -i "s/#FLAG#/$FLAG/" /var/www/localhost/htdocs/functions.js
unset FLAG

exec /usr/sbin/httpd -D FOREGROUND -f /etc/apache2/httpd.conf