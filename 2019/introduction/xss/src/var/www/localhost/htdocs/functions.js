(function() {
    var _old_alert = window.alert;
    var alertShown = false;
    var flag = "#FLAG#";
    
    window.alert = function() {
        if(!alertShown)
            document.cookie = "session=" + flag;
        
        alertShown = true;
        document.getElementById("msg").innerHTML = "You should now create a popup showing the sites cookies within it. Then we will give you the flag.";
        _old_alert.apply(window,arguments);
    };
})();