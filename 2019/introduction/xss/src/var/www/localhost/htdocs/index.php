<?php
header('X-XSS-Protection: 0');
?>
<html>
    <head>
        <title>XSS introduction</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">                
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
        <script src="functions.js"></script>
    </head>
    <body>
        <div class="pure-g">
            <div class="pure-u-1" style="height: 100px"></div>
            <div class="pure-u-1-3"></div>
            <div class="pure-u-1-3">
                <div id="msg"></div>
                <h2 style="text-align: center">Edit content</h2>
                <form class="pure-form" method="post">
                    <fieldset class="pure-group">
                        <textarea class="pure-input-1" name="content" style="height: 110px" placeholder="Please write your content here. And please do not input html-entities. But yes, &lt;b&gt;bold text&lt;/b&gt; is a good way to test for XSS. Secondly, trigger a popup and I will give you a flag in gratitude"></textarea>
                    </fieldset>
                
                <fieldset class="pure-group" style="text-align: center">
                    <button type="submit" name="save" class="pure-button pure-u-2-5 pure-button-primary">Set text</button>
                    <button type="submit" name="clear" class="pure-button pure-u-2-5 pure-button-secondary">Clear text</button>
                </fieldset>
            </form>
            </div>
            <div class="pure-u-1-3"></div>
        </div>
        <div class="pure-g">
            <div class="pure-u-1-6"></div>
            <div class="pure-u-2-3">
                <hr>
            </div>
            <div class="pure-u-1-6"></div>
        </div>
        <div class="pure-g">
            <div class="pure-u-1-3"></div>
            <div class="pure-u-1-3" style="text-align: center">
                <h2>Content</h2>
                <?php

                if(isset($_POST['clear']))
                {
                    @file_put_contents('content.txt', '');
                } else if(isset($_POST['content']))
                {
                    @file_put_contents('content.txt', $_POST['content']);
                }
                
                echo @file_get_contents("content.txt"); ?>
            </div>
            <div class="pure-u-1-3"></div>
        </div>
    </body>
</html>
