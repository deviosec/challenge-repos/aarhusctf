import base64
import hashlib
from itertools import product


enc = 'q1rge3royxrfaxnfanvzdf9hbm5vewluz30='
digest = '1f73af81f0522e57da91fdbedc8d21f07001f1f33f41da6e7bb423c92c7b79ce'


def main():
    result = []
    for chunk in zip(*[iter(enc)]*4):
        valid_variations = []
        for variation in map(''.join, product(*[(c, c.upper()) if c.isalpha() else c for c in chunk])):
            try:
                dec = base64.b64decode(variation).decode('ascii')
                if dec.isprintable():
                    valid_variations.append(dec)
            except:
                pass
            result.append(valid_variations)
    for flag in map(''.join, product(*result)):
        if hashlib.sha256(flag.encode('ascii')).hexdigest() == digest:
            print(f'flag={flag}')
            break


if __name__ == '__main__':
    main()
