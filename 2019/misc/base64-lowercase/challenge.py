import base64


flag = b'CTF{that_is_just_annoying}'


def main():
    flag_encoded = base64.b64encode(flag).decode('ascii').lower()

    with open('output.txt', 'w') as output_file:
        output_file.write('Encoded flag: {}\n'.format(flag_encoded))


if __name__ == '__main__':
    main()
