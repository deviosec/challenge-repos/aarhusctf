#!/bin/bash

python3 -c "import base64; print(base64.b32decode(\"$(cat flag.txt | base64 -d -i | xxd -p -r | base64 -d)\"))"
