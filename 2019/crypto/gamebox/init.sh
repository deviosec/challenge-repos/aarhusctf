#!/bin/sh

# plant flag 
echo ${FLAG} > flag.txt
unset FLAG

python3 shop.py &
python3 gamebox.py &

while sleep 10; do
    ps aux |grep shop |grep -q -v grep
    SHOP_STATUS=$?
    ps aux |grep gamebox |grep -q -v grep
    GAMEBOX_STATUS=$?

    # If the greps above find anything, they exit with 0 status
    # If they are not both 0, then something is wrong
    if [ $SHOP_STATUS -ne 0 -o $GAMEBOX_STATUS -ne 0 ]; then
      echo "Shop or gamebox has exited."
      exit 1
    fi
done


