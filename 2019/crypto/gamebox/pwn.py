#!/usr/bin/env python3
from binascii import unhexlify
import sys
from telnetlib import Telnet
from argparse import ArgumentParser
import threading
from pprint import pprint
from collections import defaultdict
from ecc import unserialize_sig, n
import ecc
import marshal
import base64
from gmpy2 import mpz, invert
from hashlib import sha384

parser = ArgumentParser(prog=sys.argv[0])
parser.add_argument('--host',
                    action='store',
                    default='localhost',
                    help='target host')
parser.add_argument('--port', type=int, default=9999, help='target port')
parser.add_argument('--port2', type=int, default=10000, help='second target port')
args = parser.parse_args()

HOST = args.host
PORT = args.port
PORT2 = args.port2


def prepare():
    t = Telnet(HOST, PORT)
    t.read_until(b'> ')
    t.write(b'1\n')
    t.read_until(b'> ')
    return t


def get_voucher(i, conns, vouchers):
    t = conns[i]
    t.write(b'y\n')
    t.read_until(b'Here is your free voucher:\n\n')
    vouchers[i] = t.read_until(b'\n')


def get_signatures_with_same_nonce(n=100):
    conns = [prepare() for _ in range(n)]
    vouchers = [None] * n
    threads = []
    for i in range(n):
        threads.append(
            threading.Thread(target=get_voucher, args=(i, conns, vouchers)))

    for i in range(n):
        threads[i].start()

    for i in range(n):
        threads[i].join()

    for c in conns:
        c.close()

    def destruct_voucher(v):
        v = v.decode().strip()
        v, s = v.split('-')
        v, s = unhexlify(v), unhexlify(s)
        r, s = unserialize_sig(s)
        return v, (r, s)

    sigs = defaultdict(list)
    for v, (r, s) in map(destruct_voucher, vouchers):
        sigs[r].append((v, (r, s)))
    sigs = list(filter(lambda v: len(v) > 1, sigs.values()))
    if len(sigs) == 0:
        print("[*] found no nonce reuses")
        return None
    sigs = sigs.pop()[:2]
    return sigs


def recover_sk(sigs):
    m_1, (r_1, s_1) = sigs[0]
    m_2, (r_2, s_2) = sigs[1]
    assert r_1 == r_2
    h_1 = mpz(int.from_bytes(sha384(m_1).digest(), 'big'))
    h_2 = mpz(int.from_bytes(sha384(m_2).digest(), 'big'))
    k = (h_1 - h_2) * invert((s_1 - s_2) % n, n) % n
    x = ((s_1 * k) - h_1) * invert(r_1, n) % n
    return x


def build_code(sk):
    source = """stdout_file.write(open('flag.txt', 'rb').read())"""
    rng = ecc.RNG()
    c = compile(source, 'pwn.py', 'exec')
    m = marshal.dumps(c)
    sig = ecc.serialize_sig(ecc.sign(sk, m, rng))
    return base64.b64encode(sig + m).decode()

def exec_code(code):
    t = Telnet(HOST, PORT2)
    t.read_until(b'> ')
    t.write(code.encode() + b'\n')
    t.read_until(b'==================================================\x1b[2J\x1b[H')
    flag = t.read_until(b'\n').decode().strip()
    return flag


def pwn():
    print("[+] looking for signatures using the same nonce")
    sigs = get_signatures_with_same_nonce()
    while sigs is None:
        sigs = get_signatures_with_same_nonce()
    sk = recover_sk(sigs)
    print("[+] recovered secret key")
    print("[+] signing code")
    code = build_code(sk)
    print("[+] executing code")
    flag = exec_code(code)
    print("[+] got flag:\n{}".format(flag))


if __name__ == '__main__':
    pwn()
