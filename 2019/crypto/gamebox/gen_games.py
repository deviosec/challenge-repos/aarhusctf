import marshal
import base64
import ecc
from secret import sk

files = ['date.py', 'echo.py', 'fortune.py']

rng = ecc.RNG()

def compile_and_sign(f):
    s = open(f).read()
    c = compile(s, f, 'exec')
    m = marshal.dumps(c)
    sig = ecc.serialize_sig(ecc.sign(sk, m, rng))
    return base64.b64encode(sig + m).decode()


for f in files:
    print(compile_and_sign(f))
