#!/usr/bin/env python3

from Crypto.Util.number import getPrime, isPrime, bytes_to_long
import os

flag = os.environ['FLAG_MAIN']
if not flag:
    print('No flag specifed in the environment variable')
    os.exit(1)

p = getPrime(1024)
q = p + 1
e = 65537
# make the difference slightly larger, so it takes a bit longer to solve.
skip = 100
while skip:
    q += 1
    if isPrime(q):
        if not (skip % 10):
            print('!')
        skip -= 1
N = p*q
pk = f'N={hex(p*q)}\ne={hex(e)}\n'
with open('pierre.public_key', 'w') as f:
    f.write(pk)

ptxt = bytes_to_long(bytes(flag, 'utf8'))
ctxt = pow(ptxt, e, N)
with open('flag.enc', 'w') as f:
    f.write(hex(ctxt)+'\n')
