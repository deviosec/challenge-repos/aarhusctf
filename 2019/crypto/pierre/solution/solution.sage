#!/usr/bin/env sage

# shamelessly stolen from wikipedia

def ff(N):
    a = ceil(sqrt(N))
    b2 = a*a - N
    skipped = 0
    while not is_square(b2):
        a = a + 1
        b2 = a*a - N
    return a - sqrt(b2)


def find_d(e, p, q):
    phin = (p-1)*(q-1)
    return inverse_mod(e, phin)
