from itertools import cycle


def main():
    secret = b'SKATERKITTY'

    with open('input.txt', 'r') as input_file, open('enc.bin', 'wb') as output_file:
        data = input_file.read().encode('ascii')
        output_file.write(bytes(c ^ k for c, k in zip(data, cycle(secret))))


if __name__ == '__main__':
    main()
