#!/usr/bin/env python3

from requests import get as _GET, post as _POST
from binascii import unhexlify
import argparse

p = argparse.ArgumentParser()
p.add_argument('url', help='server url')
p.add_argument('cflag', help='encrypted flag')
args = p.parse_args()

block_size = 16

# add PKCS#7 padding. Needed when we encrypt via. the padding oracle
def pad(m):
    ln = 16 - len(m) % 16
    return m + (chr(ln) * ln)


def get(path):
    return _GET(f'http://{args.url}{path}').content


def post(path, data):
    return _POST(f'http://{args.url}{path}', data=data)


def valid_padding(c):
    r = post('/getflag', {'flag': c.hex()})

    return r.status_code != 500


def decrypt_block(y, block_num):
    y = list(y)
    rs = [0] * block_size

    i = block_size - 1
    found = False
    while i >= 0:
        pb = block_size - i  # current padding byte
        print(f'[block #{block_num}] {"."*pb}', end='\r')
        rh = rs[:i]
        rt = [c ^ pb for c in rs[i + 1:]]

        for j in range(256):
            r = rh + [j] + rt
            if valid_padding(bytes(r + y)):
                rs[i] = pb ^ j
                found = True
                break
        i -= 1

        if not found:
            print(f'[!] nothing found for pb={pb}, rs[{i}]={rs[i]}')

        found = False

    print()
    return rs


def encrypt_block(t, c, i):
    t = [ord(c) for c in t]
    a = decrypt_block(c, i)
    b = ''.join(chr(c1 ^ c2) for c1, c2 in zip(a, t))
    return bytes([ord(i) for i in b])


def decrypt(cflag):
    iv = cflag[:block_size]
    cflag = cflag[block_size:]

    blocks = []
    for i in range(0, len(cflag), block_size):
        blocks = [cflag[i:i+block_size]] + blocks

    print(f'[ ] {len(blocks)} blocks')

    msg = ''
    i = 0
    while i < len(blocks) - 1:
        d = decrypt_block(blocks[i], len(blocks) - i)
        if d is None:
            continue
        else:
            msg = ''.join(chr(c1 ^ c2) for c1, c2 in zip(d, blocks[i+1])) + msg
            i += 1

    d = decrypt_block(blocks[-1], 1)
    msg = ''.join(chr(c1 ^ c2) for c1, c2 in zip(d, iv)) + msg

    ptxt = msg.strip()

    print(f'[ ] plaintext={ptxt}')


if __name__ == '__main__':
    decrypt(unhexlify(args.cflag))
