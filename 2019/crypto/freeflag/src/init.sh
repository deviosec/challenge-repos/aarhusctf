#!/bin/sh
# save out flag to a file
echo $FLAG > flag.txt
unset FLAG

# start our webserver
python3 -m flask run --host ${FLASK_HOST}
