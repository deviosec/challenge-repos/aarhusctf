from flask import Flask
from flask import request
from Crypto.Cipher import AES
from os import urandom as rand
from binascii import unhexlify

app = Flask(__name__)

with open('flag.txt') as f:
    flag = f.read().strip()

key = rand(32)  # AES key

# Creates an AES cipher object
def aes(iv):
    return AES.new(key, AES.MODE_CBC, iv)


# apply PKCS#7 padding to m
def pad(m):
    ln = 16 - len(m) % 16
    return m + (chr(ln) * ln)


# remove PKCS#7 padding. Exposes a padding oracle
def unpad(m):
    ln = m[-1]
    for c in m[-ln:]:
        if c != ln:
            return
    if 0 < ln <= 16:
        return m[:-ln]


def decrypt(c):
    c = unhexlify(c)

    iv = c[:16]
    c = c[16:]

    if len(c) % 16:
        return

    return unpad(aes(iv).decrypt(c))

@app.route('/')
def init():
    iv = rand(16)
    c = aes(iv).encrypt(pad(flag))
    s = '<html><body><h1>Here\'s your encrypted flag (IV prepended):</h1>'
    s += f'<p>{iv.hex()}{c.hex()}</p></body></html>'
    return s

@app.route('/getflag', methods=['GET', 'POST'])
def getflag():
    if request.method == 'GET':
        return ('<html><body><form action="/getflag" method="POST">'
                'encrypted flag:<input type="text" name="flag">'
                '<input type="submit" value="submit">'
                '</form></body></html>')
    if request.method == 'POST':
        data = request.form
        if 'flag' in data:
            flag = decrypt(data['flag'])
        if flag is None:
            return '', 500
        return '<html><body><h1>decryption is not available</h1></body></html>'

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
