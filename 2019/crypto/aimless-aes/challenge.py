import binascii
import os

from Crypto.Cipher import AES
import key_schedule

flag = b"CTF{_r0und_k3ys}"


def main():
    key = os.urandom(16)
    cipher = AES.new(key, AES.MODE_ECB)

    with open("enc.bin", "wb") as output_file_data, open(
        "key.txt", "w"
    ) as output_file_text:
        output_file_data.write(cipher.encrypt(flag))

        for i in range(7):
            key = key_schedule.next_round_key(key, i)

        key = key_schedule.next_round_key(key, 7)
        key_str = binascii.hexlify(bytes(key)).decode("ascii")
        key_str = key_str[0:8] + "?" * 16 + key_str[24:]
        output_file_text.write("Round key {}:  {}\n".format(8, key_str))

        key = key_schedule.next_round_key(key, 8)
        key_str = binascii.hexlify(bytes(key)).decode("ascii")
        key_str = "?" * 2 + key_str[2:12] + "?" * 4 + key_str[16:28] + "?" * 4
        output_file_text.write("Round key {}:  {}\n".format(9, key_str))

        key = key_schedule.next_round_key(key, 9)
        key_str = binascii.hexlify(bytes(key)).decode("ascii")
        key_str = key_str[0:16] + "?" * 16
        output_file_text.write("Round key {}: {}\n".format(10, key_str))


if __name__ == "__main__":
    main()
