from Crypto.Cipher import AES

import key_schedule


def main():
    round_key = bytes.fromhex("eacdefe96c19721e2e57ada17c3d6292")
    for i in range(9, 0, -1):
        round_key = key_schedule.previous_round_key(round_key, i)

    cipher = AES.new(bytes(round_key), AES.MODE_ECB)

    with open("enc.bin", "rb") as input_file:
        print("Flag: {}".format(cipher.decrypt(input_file.read()).decode("utf-8")))


if __name__ == "__main__":
    main()
