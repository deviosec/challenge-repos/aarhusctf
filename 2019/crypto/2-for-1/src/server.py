from Crypto.Util.number import bytes_to_long, long_to_bytes, getRandomInteger, GCD
from os import urandom
import socket

max_n_size = 2048

with open('flag.txt') as f:
    flag = bytes(f.read().strip(), 'ascii')
    print('read flag')


def validate_n(n):
    # n must not be too large
    n = int(n.rstrip())
    n2 = pow(n, 2)
    assert n.bit_length() <= max_n_size, 'key too large'
    return n, n2


def validate_g(g, n2):
    # g must be at most |n^2| bits.
    g = int(g.rstrip())
    assert g.bit_length() <= n2.bit_length(), 'g too large'
    return g


def validate_cb(cb, n2):
    # a ciphertext must be a valid integer mod n^2.
    cb = int(cb.rstrip())
    assert 0 <= cb < n2, 'invalid message'
    return cb


def OT(g, n, n2, cb):
    while True:
        # new random key
        key = urandom(len(flag))
        # encrypt flag
        enc_flag = bytes((x ^ y for x, y in zip(flag, key)))

        # Set messages. If it happens that m1 > m0, we just try again (otherwise
        # the pow function complains about a negative exponent).
        m0 = bytes_to_long(enc_flag)
        m1 = bytes_to_long(key)
        if m0 <= m1:
            break

    # compute Enc(pk, m0)
    while True:
        r = getRandomInteger(n2.bit_length())
        if GCD(r, n) == 1:
            break
    # ct0 = Enc(pk, m0)
    ct0 = (pow(g, m0, n2) * pow(r, n, n2)) % n2

    # ctb = cb^(m1 - m0) * Enc(pk, m0)
    #     = Enc(pk, b)^(m1 - m0) * Enc(pk, m0)
    #     = Enc(pk, b(m1 - m0) + m0)         // Paillier is additively homomorphic
    #     = Enc(pk, mb)
    ctb = (pow(cb, (m1 - m0), n2) * ct0) % n2
    return ctb


host = ''
port = 12001

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind((host, port))
while True:
    sock.listen(1)
    conn, addr = sock.accept()
    print(f'connection from {addr}')
    conn.sendall((b'Hello, and welcome to the Oblivious Transfer portal\n'
                  b'I have two messages, one of which you may have:\n'
                  b'  m0 = Enc(k, flag) = k XOR flag\n'
                  b'  m1 = k\n'))
    conn.sendall((b'\n'
                  b'To receive a message, input you Paillier public key now:\n'))
    try:
        # obtain client public key (n, g)
        conn.sendall(b'n> ')
        n = conn.recv(2048)
        conn.sendall(b'g> ')
        g = conn.recv(2048)
        n, n2 = validate_n(n)
        g = validate_g(g, n2)
    except Exception as e:
        print(e)
        conn.sendall(b'oh no invalid key :-(\n')
        conn.close()
        continue
    conn.sendall((b'Thank you!\n'
                  b'Next, send me your encrypted choice bit\n'
                  b'That is, cb = Enc(pk, 0) or cb = Enc(pk, 1)\n'
                  b'(But don\'t tell us which one)\n'))
    try:
        # get client's encrypted choice message
        conn.sendall(b'cb> ')
        cb = conn.recv(2048)
        cb = validate_cb(cb, n2)
        # compute OT response
        ctb = OT(g, n, n2, cb)
        conn.sendall(b'Here\'s your message:\n')
        conn.sendall(bytes(str(ctb), 'ascii'))
    except Exception as e:
        print(e)
        conn.sendall(b'oh no. I broke :-(\n')
        conn.close()
        continue

    conn.sendall(b'\nbye!\n')
    conn.close()
