#!/usr/bin/env python2

from helpers import *
from pwn import *
import sys

if len(sys.argv) != 3:
    print("usage: python solution.py <host> <port>")
    exit(1)

host = sys.argv[1]
port = int(sys.argv[2])

# no need to be exact here. Just needs to be larger
FLAG_LENGTH_BITS = 300

pk, sk = gen_key()

r = remote(host, port)
r.recvuntil('n> ')
print('[*] sending n')
r.send(str(pk[0]))
r.recvuntil('g> ')
print('[*] sending g')
r.send(str(pk[1]))
e = encrypt(pk, pow(2, FLAG_LENGTH_BITS))
r.recvuntil('cb> ')
print('[*] sending choice')
r.send(str(e))
r.recvline()  # "here's your message"
ctxt = decrypt(pk,sk,int(r.recvline().strip()))
top = ctxt >> FLAG_LENGTH_BITS
bot = bytes2int(int2bytes(ctxt)[-(FLAG_LENGTH_BITS//8):])
top += bot
print('[!]', ''.join([chr(x^y) for x,y in zip(int2bytes(top), int2bytes(bot))]))
