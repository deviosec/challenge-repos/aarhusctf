#!/usr/bin/python3

from Crypto.Cipher import ARC4
from Crypto.Cipher import AES
from binascii import hexlify
import os


NUM_ROUNDS = 42
BLOCKS_PER_QUERY = 32


def use_arc4():
    key = os.urandom(16)
    arc4 = ARC4.new(key)
    return arc4.encrypt(b'\x00' * 16)


def use_aes():
    key = os.urandom(16)
    aes = AES.new(key, AES.MODE_ECB)
    return aes.encrypt(b'\x00' * 16)


def round(i):
    print("===== Round {} =====".format(i))
    print("Is this AES or RC4?")
    b = os.urandom(1)[0] % 2
    cipher = ["aes", "rc4"][b]
    gen = [use_aes, use_arc4][b]
    while True:
        for i in range(BLOCKS_PER_QUERY):
            data = gen()
            print(hexlify(data).decode())
        inp = input("> ").strip()
        while inp not in ["more", "aes", "rc4"]:
            print("???")
            inp = input("> ").strip()
        if inp == "more":
            continue
        elif inp == cipher:
            print("Correct")
            return True
        else:
            print("Wrong")
            return False


def main():
    print("Welcome to the distinguishing game!")

    for i in range(NUM_ROUNDS):
        if not round(i):
            print("Game Over!")
            break
    else:
        flag = open('flag.txt').read()
        print("Congratulations!")
        print("Here is your flag: {}".format(flag))


if __name__ == '__main__':
    main()
