#!/usr/bin/env python3

from argparse import ArgumentParser
from binascii import unhexlify
from telnetlib import Telnet
import sys

parser = ArgumentParser(prog=sys.argv[0])
parser.add_argument('--host',
                    action='store',
                    default='localhost',
                    help='target host')
parser.add_argument('--port', type=int, default=1337, help='target port')
args = parser.parse_args()

HOST = args.host
PORT = args.port

NUM_ROUNDS = 42
BLOCKS_PER_QUERY = 32


def main(t):
    samples = 256 * 30
    assert samples % BLOCKS_PER_QUERY == 0
    prob_aes = 1 / 256
    prob_rc4 = 1 / 128
    expected_aes = samples * prob_aes
    expected_rc4 = samples * prob_rc4
    threshold = (expected_aes + expected_rc4) // 2
    print("Expected number of zero bytes for RC4: {}".format(expected_rc4))
    print("Expected number of zero bytes for AES: {}".format(expected_aes))
    for i in range(NUM_ROUNDS):
        print("Round {}: ".format(i + 1), end='')
        data = []
        t.read_until(b'Is this AES or RC4?\n')
        for _ in range(BLOCKS_PER_QUERY):
            block = unhexlify(t.read_until(b'\n')[:-1])
            data.append(block)
        t.read_until(b'> ')
        for _ in range(samples // BLOCKS_PER_QUERY - 1):
            t.write(b'more\n')
            for _ in range(BLOCKS_PER_QUERY):
                block = unhexlify(t.read_until(b'\n')[:-1])
                data.append(block)
            t.read_until(b'> ')
        #  print(data)
        count = sum(1 for block in data if block[1] == 0)
        print("Got {} zero bytes -> {}".format(
            count, 'aes' if count < threshold else 'rc4'))
        if count < threshold:
            t.write(b'aes\n')
        if count >= threshold:
            t.write(b'rc4\n')
        response = t.read_until(b'\n').strip()
        if response == b'Wrong':
            print("Wrong answer :(")
            exit(1)
    t.read_until(b'Here is your flag: ')
    flag = t.read_until(b'\n').decode().strip()
    return flag


if __name__ == '__main__':
    t = Telnet(HOST, PORT)
    flag = main(t)
    print(flag)
