#!/usr/bin/env python2

# import socket

from pwn import *
from sys import argv
from functools import reduce
from fractions import gcd

N = 10
q = 5
rounds = 50

r = remote('localhost', 12003)
r.recvuntil('?> ')
r.send('3')
print r.recvline()   # "50 rounds left. Good luck!"

def parse_data():
    data = r.recvuntil('v> ').split('\n')
    T2 = [[int(v) for v in x.split(',')] for x in data[:N]]
    M = [[int(v) for v in x.split(',')] for x in data[N:2*N]]
    s = int(data[2*N])
    u = int(data[2*N+1])
    return T2, M, s, u


def reverse_rng(M):
    mflat = list()
    for i in range(N):
        for j in range(N):
            mflat.append(M[i][j])
    ts = [mflat[i+1]-mflat[i] for i in range(len(mflat)-1)]
    us = [ts[i+2]*ts[i]-ts[i+1]**2 for i in range(len(ts)-2)]
    p = abs(reduce(gcd, us))  # dunno why it's sometimes negative
    print '[*] p =', p
    x0 = mflat[0]
    x1 = mflat[1]
    x2 = mflat[2]
    inv = pow((x1 - x0)%p, p-2, p)
    a = ((x2 - x1)*inv) % p
    print '[*] a =', a
    b = (x2 - a*x1) % p
    print '[*] b =', b
    seed = (((x0 - b))*pow(a,p-2,p)) % p
    print '[*] seed =', seed
    return seed, a, b, p

def compute_mac_vals(M, T, seed, a, b, p):
    def rng():
        global seed
        seed = (seed*a + b) % p
        return seed
    # skip
    for i in range(N):
        for j in range(N):
            r = rng()
            m = M[i][j]
            assert r == m, 'r=%s, m=%s (%s,%s)' % (r, m, i,j)
    A = [[rng() for _ in range(N)] for _ in range(N)]
    B = [[(M[i][j] - A[i][j]*T[i][j]) % p for j in range(N)]
         for i in range(N)]
    return A, B

for i in range(rounds):
    print '[*] round %s' % i
    T2, M, s, u = parse_data()
    seed, a, b, p = reverse_rng(M)
    A, B = compute_mac_vals(M, T2, seed, a, b, p)
    v = 0  # input dont matter
    T2uv = (T2[u][v] + 1) % q
    m = (T2uv*A[u][v] + B[u][v]) % p
    print '[*] sending bogus v'
    r.send(str(v))
    r.recvuntil('T2[u][v]> ')
    print '[*] sending malformed share T2uv = %s' % T2uv
    r.send(str(T2uv))
    r.recvuntil('M[u][v]> ')
    print '[*] sending mac = %s' % m
    r.send(str(m))

# flag
print r.recvline()
