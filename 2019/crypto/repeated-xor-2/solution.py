from itertools import cycle

ptxt = b'GIF89a\xf4\x01\x19\x01'

with open('flag.bin', 'rb') as f, \
     open('flag_decrypted.gif', 'wb') as g:

    # recover key
    flag = f.read()
    key = bytes(a ^ b for a, b in zip(flag[:10], ptxt))

    print('[*] found key', key)
    print('[*] writing flag_decrypted.gif')

    g.write(bytes(a ^ b for a, b in zip(flag, cycle(key))))
