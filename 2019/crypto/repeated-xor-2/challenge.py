from itertools import cycle

key = b'key=Banana'

with open('flag.gif', 'rb') as f, \
     open('flag.bin', 'wb') as g:
    flag = f.read()
    g.write(bytes(a ^ b for a, b in zip(flag, cycle(key))))
