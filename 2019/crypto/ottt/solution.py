#!/usr/bin/env python2

# import socket

from pwn import *
from sys import argv

N = 10
p = 626067822350698667
rounds = 50

r = remote('localhost', 12002)
r.recvuntil('?> ')
r.send('3')
r.recvline()   # "50 rounds left. Good luck!"

for i in range(rounds):
    print '[*] round %s' % i
    data = r.recvuntil('v> ').split('\n')[:-1]
    T2 = [[int(v) for v in x.split(',')] for x in data[:N]]
    s = int(data[N])
    u = int(data[N+1])
    T2uv = (T2[u][0] + 1) % p
    #T2uv =1 
    print '[*] sending bogus v'
    r.send('0')
    r.recvuntil('T2[u][v]> ')
    print '[*] sending malformed share T2uv = %s' % T2uv
    r.send(str(T2uv))

# flag
print r.recvline()
