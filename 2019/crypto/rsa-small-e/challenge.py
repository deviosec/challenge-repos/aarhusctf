from Crypto.PublicKey import RSA


flag = b'CTF{r3memb3r_2_p4d_RSA_:)}'
flag_num = int.from_bytes(flag, byteorder='big')


def main():
    key = RSA.generate(2048, e=3)
    encrypted_flag = pow(flag_num, key.e, key.n)

    with open('key.txt', 'w') as output_file:
        output_file.write('e: {}\n'.format(key.e))
        output_file.write('n: {}\n'.format(key.n))
        output_file.write('c: {}\n'.format(encrypted_flag))


if __name__ == '__main__':
    main()
