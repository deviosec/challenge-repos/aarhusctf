from sage.crypto.util import bin_to_ascii

with open('key.txt') as f:
    k = f.read().split('\n')
    e = int(k[0].strip()[2:])
    n = int(k[1].strip()[2:])
    c = int(k[2].strip()[2:])

flag = c^(1/e) % n

flag = bin(flag)
flag = (8 - (len(flag[2:]) % 8)) * '0' + flag[2:]
print(bin_to_ascii(flag))
