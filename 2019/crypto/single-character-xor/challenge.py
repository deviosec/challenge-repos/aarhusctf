flag = b'CTF{H4rdly_4_s3cur3_c1ph3r}'

def main():
    secret = ord('Q')

    with open('enc.bin', 'wb') as output_file:
        encrypted_flag = bytes(c ^ secret for c in flag)
        output_file.write(encrypted_flag)


if __name__ == '__main__':
    main()
