def main():
    with open('enc.bin', 'rb') as input_file:
        data = input_file.read()
        for i in range(256):
            decrypted_flag = bytes([c ^ i for c in data])
            if b'CTF' in decrypted_flag:
                print('Flag: {}'.format(decrypted_flag))
                print('Key: {}'.format(chr(i)))
                break


if __name__ == '__main__':
    main()
