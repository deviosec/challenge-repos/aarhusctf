<?php
function check_query($query) {
    $bad_words = array('analyze', 'database', 'transaction', 'load_extension', 'randomblob', 'zeroblob', 'pragma', 'savepoint', 'vacuum');

    foreach ($bad_words as $bad_word) {
        if (stripos($query, $bad_word) !== FALSE)
        {
            return $bad_word;
        }
    }

    return FALSE;
}
?>

