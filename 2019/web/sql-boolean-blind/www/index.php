<?php
require 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Leaked City</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Oxygen:400,300,700" rel="stylesheet" type="text/css"/>
    <link href="https://code.ionicframework.com/ionicons/1.4.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
</head>
<body>
    
<div id="app">
  <div class="signin cf">
	<div class="avatar"></div>
	<h1>Leak Database</h1>
<?php
if (isset($_POST['pass'])) {
	$db = new SQLite3('/var/db/data.db');
	
	$query = "SELECT password FROM leaked_passwords WHERE password='{$_POST['pass']}'";
	
	if(stripos($_SERVER["HTTP_USER_AGENT"], "curl") !== false || stripos($_SERVER["HTTP_USER_AGENT"], "sqlmap") !== false)
		sleep(10);
		
	$bad_word = check_query($query);
	if ($bad_word) {
		echo "<p>The query contains the bad word:</p><pre>$bad_word</pre>";
	}
	else {
		$results = $db->query($query);
		if ($results === FALSE) {
			echo "<p>The SQLite query failed.</p>";
		}
		else {
			$result = $results->fetchArray(SQLITE3_ASSOC);
			if ($result !== FALSE) {
				echo "<p>Oh no, your password has been leaked :'(</p>";
			}
			else {
				echo "<p>Congratulations, your password is safe! :D</p>";
			}
		}
	}
}
?>
		<p>Welcome to the leak database! Use the form below to check if your password has been leaked:</p>
        <form method="post" autocomplete="off" id="formdata"> 
      <div class="inputrow">
        <input type="password" id="pass" placeholder="Password" autocomplete="off" name="pass" />
        <label class="ion-locked" for="pass"></label>
      </div>
      <input type="submit" value="Check" />
    </form>
      </div>
</div>


</body>
</html>

