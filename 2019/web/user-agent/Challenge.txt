Title: Secret Agent
Description: Can you infiltrate this site and find the flag?
Flag: CTF{4ny_1npu7_c4n_b3_m4n1pul473d!}
Hint: The user agent string can easily be spoofed.
Difficulty: Easy
Files: -
