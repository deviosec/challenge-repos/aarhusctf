<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<title>Browsers</title>
</head>
<body>
        <div class="container">
            <div class="content">
<?php
function frontpage() {
	echo "<h1>Welcome to the browser site!</h1>\r\n";
	echo "<p>Select your browser below for more information:</p>\r\n";
	echo "<ul>\r\n";
	echo "<li><a href=\"?browser=chrome\">Chrome</a></li>\r\n";
	echo "<li><a href=\"?browser=edge\">Edge</a></li>\r\n";
	echo "<li><a href=\"?browser=firefox\">Firefox</a></li>\r\n";
	echo "<li id=\"hidden\"><a href=\"?browser=ultron\">Ultron</a></li>\r\n";
	echo "</ul>\r\n";
}

function access_denied() {
	echo "<h1>Sorry, your browser does not seem to match this page :(</h1>\r\n";
}

function print_user_agent() {
	echo "<p>Your user agent string is: {$_SERVER['HTTP_USER_AGENT']}</p>\r\n";
}

if (!isset($_GET['browser'])) {
	frontpage();
}
else if ($_GET['browser'] === 'chrome') {
	if (stripos($_SERVER['HTTP_USER_AGENT'], 'chrome') !== FALSE) {
		echo "<h1>Chrome</h1>\r\n";
		print_user_agent();
		echo "<p>Google Chrome (commonly known simply as Chrome) is a cross-platform web browser developed by Google. It was first released in 2008 for Microsoft Windows, and was later ported to Linux, macOS, iOS, and Android. The browser is also the main component of Chrome OS, where it serves as the platform for web apps.</p>\r\n";
	}
	else {
		access_denied();
	}
}
else if ($_GET['browser'] === 'edge') {
	if (stripos($_SERVER['HTTP_USER_AGENT'], 'edge') !== FALSE) {
		echo "<h1>Edge</h1>\r\n";
		print_user_agent();
		echo "<p>Microsoft Edge (codename 'Spartan') is a web browser developed by Microsoft. It was first released for Windows 10 and Xbox One in 2015, then for Android and iOS in 2017.</p>\r\n";
	}
	else {
		access_denied();
	}
}
else if ($_GET['browser'] === 'firefox') {
	if (stripos($_SERVER['HTTP_USER_AGENT'], 'firefox') !== FALSE) {
		echo "<h1>Firefox</h1>\r\n";
		print_user_agent();
		echo "<p>Mozilla Firefox (or simply Firefox) is a free and open-source web browser developed by The Mozilla Foundation and its subsidiary, Mozilla Corporation. Firefox is available for Windows, macOS, Linux, BSD, illumos and Solaris operating systems. Its sibling, Firefox for Android, is also available. Firefox uses the Gecko layout engine to render web pages, which implements current and anticipated web standards. In 2017, Firefox began incorporating new technology under the code name Quantum to promote parallelism and a more intuitive user interface. An additional version, Firefox for iOS, was released on November 12, 2015. Due to platform restrictions, it uses the WebKit layout engine instead of Gecko, as with all other iOS web browsers.</p>\r\n";
	}
	else {
		access_denied();
	}
}
else if ($_GET['browser'] === 'ultron') {
	if (stripos($_SERVER['HTTP_USER_AGENT'], 'ultron') !== FALSE) {
		echo "<h1>Ultron</h1>\r\n";
		print_user_agent();
		echo "<p>Well done agent: CTF{4ny_1npu7_c4n_b3_m4n1pul473d!}</p>\r\n";
	}
	else {
		access_denied();
	}
}
else {
	frontpage();
}
?>
</div>
</div>
</body>
</html>

