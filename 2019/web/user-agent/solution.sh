#!/bin/bash

site=$1

if [ -z "$site" ]; then
    echo "usage: $0 [url]"
    exit 1
fi

curl -s -A 'ultron' "$site/?browser=ultron" | grep -ioE 'ctf\{.+\}'
