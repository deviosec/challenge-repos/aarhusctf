<?php
if (!isset($_COOKIE['admin'])) {
	setcookie('admin', 'no');
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Flag Vault</title>
</head>
<body>
<?php
if ($_COOKIE['admin'] === 'yes') {
	echo "<h1>ACCESS GRANTED</h1>\r\n";
	echo "<p>Welcome! Here is a flag: CTF{never_trust_a_client}</p>\r\n";
}
else {
	echo "<h1>ACCESS DENIED</h1>\r\n";
	echo "<p>You do not have permission to view this page :(</p>\r\n";
}
?>
</body>
</html>

