<!DOCTYPE html>
<html>
<head>
<title>Another Login Page</title>
</head>
<body>
<h1>Yet Another Login Page</h1>
<p>Ready, set, go!</p>
<form method='post'>
<p>Password</p><input type='password' name='pass'>
<br><br><input type='submit'>
</form>
<?php
if (isset($_POST['pass'])) {
	$pass = 'noice';
	$pass_len = strlen($pass);
	$input_len = strlen($_POST['pass']);
	
	for ($i = 0; $i < $pass_len; $i++) {
		if ($i === $input_len || $pass[$i] !== $_POST['pass'][$i]) {
			echo "<p>Nope!</p>";
			exit();
		}
		else {
		    usleep(10000);
		}
	}
	echo "<p>YES! Well, done: CTF{gotta_go_fast}</p>";
}
?>
</body>
</html>

