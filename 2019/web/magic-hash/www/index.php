<?php	
if (isset($_GET['source'])) {
	highlight_file(__FILE__);
	exit;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Magic</title>
<link rel="stylesheet" href="style.css">
</head>
<body>	
	<div class="container">
		<div>
			<div class="box">
				<h1>Admin Login</h1>
				<p>Good luck getting through this extremely secure login page unless you know the password. It is so secure that you can have the <a href='?source'>source!</a></p>
				<form method='post'>
					<label>
							<p>Password:</p><input type='password' name='pass'>
					</label>
					
					<input type='submit' value='Login'>
				</form>

				<?php
				if (isset($_POST['pass'])) {
					var_dump(md5($_POST['pass']));
					if (md5($_POST['pass']) == '0e396450196960173183811778704347') {
						echo "<p>Welcome admin! Here is your flag: ".file_get_contents('/var/flag.txt')."</p>\r\n";
					}
					else {
						echo "<p>Go away!</p>\r\n";
					}
				}
				?>
			</div>
		</div>
	</div>
</div>
</body>
</html>

