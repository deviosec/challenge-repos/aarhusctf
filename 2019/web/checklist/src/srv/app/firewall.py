from flask import *
from werkzeug.exceptions import NotFound
from functools import wraps


def firewall(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        keys = [a for a in dir(request) if not a.startswith('__')]

        for k in keys:
            if "flag" in str(getattr(request, k)).lower():
                return "Killed by firewall!"

        return f(*args, **kwargs)
    return decorated_function
