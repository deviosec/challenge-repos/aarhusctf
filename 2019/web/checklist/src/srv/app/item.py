import itertools


class Item:
    newid = itertools.count().__next__

    def __init__(self, name):
        self.id = Item.newid()
        self.name = name
