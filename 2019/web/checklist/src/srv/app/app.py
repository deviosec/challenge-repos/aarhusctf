from flask import *
from item import Item
from firewall import *

app = Flask(__name__)
#app.wsgi_app = Firewall(app.wsgi_app)
items = {}

def _add_item(label):
    item = Item(label)
    items[item.id] = item

_add_item("Create a plan")
_add_item("Follow the plan")
_add_item("Try something else")



@app.route('/')
def index():
    return render_template_string('''
{% extends "layout.html" %}
{% block content %}
<form>
  {% for _, item in items.items() %}
  <span class="todo-wrap">
    <input type="checkbox" id="{{ item.id }}" />
    <label for="{{ item.id }}" class="todo">
      <i class="fa fa-check"></i>
      {{ item.name }}
    </label>
      <a class="delete-item" title="Remove" href="/confirm_delete/{{ item.id }}"><i class="fa fa-times-circle"></i></a>
  </span>
  {% endfor %}
  <div id="add-todo">
    <i class="fa fa-plus"></i>
    <a href="{{ url_for('add_page') }}" class="button-link">Add an item</a>
  </div>
</form>
{% endblock %}
''', items=items)

@app.route('/add')
def add_page():
    return render_template_string('''
{% extends "layout.html" %}
{% block content %}
<form method='post'>
  <span class="row">
    <textarea name="label" class="input-todo"></textarea>
  </span>
  <div id="add-todo">
    <input type="submit" name="cancel" class="button" value="Cancel" />
    <input type="submit" name="save" class="button" value="Save" />
  </div>
</form>

{% endblock %}
''')

@app.route('/confirm_delete/<id>')
@firewall
def confirm_delete(id):
    id = int(id)

    already_deleted = id not in items
    if already_deleted:
        return redirect(url_for('index'))

    return render_template_string('''
<script>
if(confirm("Do you really want to delete item '%s'?"))
   window.location = '/delete/%s';
else
   window.location = '/';
</script>
''' % (items[id].name, id))

@app.route('/delete/<id>')
@firewall
def delete_item(id):
    id = int(id)
    if id in items:
        del items[id]

    return redirect(url_for('index'))

@app.route('/add', methods=['post'])
@firewall
def store_item():
    if "cancel" not in request.form:
        _add_item(request.form["label"])

    return redirect(url_for('index'))

