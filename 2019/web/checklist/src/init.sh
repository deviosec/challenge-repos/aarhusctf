#!/usr/bin/env sh

# setup our flag, and purge it after
if [ ! -z $FLAG ]; then
    echo $FLAG > /srv/app/flag.txt
    unset FLAG
    exec sh /init.sh
fi

# start our webserver
python -m flask run --host ${FLASK_HOST}
