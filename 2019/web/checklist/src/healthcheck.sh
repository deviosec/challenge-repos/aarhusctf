#!/usr/bin/env sh

# check that the server is alive and active
URL="http://127.0.0.1:5000"
wget $URL -O -
if [ $? -ne 0 ]; then 
    exit 1
fi
exit 0

# make sure that the flag is still there (not deleted)
ls /srv/app/flag.txt
if [ $? -ne 0 ]; then 
    exit 1
fi
exit 0
