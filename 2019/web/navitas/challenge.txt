id: navitas
title: Navitas
description: The linux system has a user with a quite special username. Maybe you can find that?
flags:
  - name: MAIN
    flag: CTF{I_sh0Uld_n07_t4k3_P4thS_fR0M_t3H_U53R}
    dynamic_env: true
hints:
  - hint: This is called Local File Inclusion (LFI). However it seems the path has some kind of prefix.
    cost: 100
category: web
tags:
    lfi: true
    path-traversal: true
difficulty: 10
lab:
  virtuals:
    - envs:
      - FLAG=##FLAG_MAIN##
      ports:
        HTTP:
          guest: 80
