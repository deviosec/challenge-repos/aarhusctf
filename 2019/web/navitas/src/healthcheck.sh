#!/usr/bin/env sh

# check if website is alive and accessible
URL="http://127.0.0.1/"
wget $URL -O -
if [ $? -ne 0 ]; then 
    exit 1
fi
exit 0

# check that the flag is in the right place
# which we do, by just greping after userid with 1000
grep 1000 /etc/passwd
if [ $? -ne 0 ]; then 
    exit 1
fi
exit 0
