<!-- Banner -->
<div class="banner">
	    <div class="page-container uk-grid">
		<div class="banner-background uk-width-1-2">
		    <div class="padding">
			
			<h1>Welcome stranger!</h1>
			<p>
			   We are tracking and logging all requests to improve security. You've been warned!
			</p>
			<p>
			   Your IP is: <br />
			   <span class="tracking-info"><?= $_SERVER['REMOTE_ADDR']; ?></span>
			</p>
			<p>
			   Your User Agent is: <br />
			   <span class="tracking-info"><?= htmlentities($_SERVER['HTTP_USER_AGENT']); ?></span>
			</p>
		    </div>
		</div>
  	    </div>
	</div>
	<!-- End banner -->

	<!-- Main content -->
	<div id="main" class="page-container">
		<article>

			<div class="uk-grid uk-grid-width-1-3" data-uk-grid-margin>
			    <div>
                                <div class="feature-icon">
                                    <i class="uk-icon-fire"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Velit Esse</h4>
				    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit</p>
                                </div>
                            </div>
                            <div class="">
                                <div class="feature-icon">
                                    <i class="uk-icon-magic"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Cillum Dolore</h4>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                                </div>
                            </div>
                            <div class="">
                                <div class="feature-icon">
                                    <i class="uk-icon-flag"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Flag me</h4>
				    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit</p>
                                </div>
                            </div>

                            <div>
                                <div class="feature-icon">
                                    <i class="uk-icon-cloud"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Duis Aute Irure</h4>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                                </div>
                            </div>
                            <div>
                                <div class="feature-icon">
                                    <i class="uk-icon-ok"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Voluptate Velit</h4>
				    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit</p>
                                </div>
                            </div>
                            <div>
                                <div class="feature-icon">
                                    <i class="uk-icon-twitter"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Sunt In Culpa</h4>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                                </div>
                            </div>
                        </div>
		</article>
	</div>
	<!-- End main content -->