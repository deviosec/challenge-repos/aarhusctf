	<!-- Main content -->
	<div id="main" class="page-container">
		<article>

            <h2>About Aarhus University</h2>
<p>The university was founded in 1928 and today it has several world class research fields. 

Aarhus University (AU) is a top ten university among universities founded within the past 100 years. It has a long tradition of partnerships with some of the world's best research institutions and university networks.
</p>
<p>AU has a strong commitment to the development of society that is realised through its collaboration with government agencies and institutions and the business community. 

</p>
<p>
The university’s goal is to contribute towards solving the complex global challenges facing the world. The university therefore strives to combine the high level of academic standards of its researchers with collaboration across disciplinary boundaries to combine research in new ways and solve challenges in close contact with the world around us.
</p>

			<div class="uk-grid uk-grid-width-1-2" data-uk-grid-margin>
			    <div>
                                <div class="feature-icon">
                                    <i class="uk-icon-fire"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Velit Esse</h4>
				    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit</p>
                                </div>
                            </div>
                            <div class="">
                                <div class="feature-icon">
                                    <i class="uk-icon-magic"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Cillum Dolore</h4>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                                </div>
                            </div>
                            <div class="">
                                <div class="feature-icon">
                                    <i class="uk-icon-flag"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Flag me</h4>
				    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit</p>
                                </div>
                            </div>

                            <div>
                                <div class="feature-icon">
                                    <i class="uk-icon-cloud"></i>
                                </div>
                                <div class="feature-text">
                                    <h4>Duis Aute Irure</h4>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                                </div>
                            </div>
                        </div>
		</article>
	</div>
	<!-- End main content -->