<?php

$flags = (object)[
];
header('Job: We are currently searching for developers and hackers alike. If you see this message, please call us and tell us about your insane skills!');

$page = (isset($_GET['page']) ? $_GET['page'] : NULL);
$path = dirname(__FILE__) . '/pages/';

if(empty($page) || !is_file($path . $page))
	$page = 'home.php';

$menu = [
	'home.php' => 'Home',
	'about.php' => 'About us'
];


?>
<!DOCTYPE html>
<html>
    <head>
        <title>AU Navitas</title>
        <link rel="stylesheet" href="/css/uikit.min.css" />
        <script src="/js/jquery.min.js"></script>
		<script src="/js/uikit.js"></script>
		<link rel="stylesheet" href="/css/style.css" />
    </head>
    <body>

	<!-- 
	// TODO: Remove comments before publishing
	-->

	<div id="wrapper">

  	    <div class="header uk-grid page-container">
                <div class="logo uk-width-1-3">
                    <h2><a href="/">Navitas</a></h2>
                </div>
                <div class="tel-skype uk-width-2-3">
				
				<ul class="menu">
					<?php
					foreach($menu as $file => $label)
					{
						echo '<li' . ($page === $file ? ' class="uk-active"' : '') . '><a href="?page=' . $file . '">' . $label . '</a></li>';
					}
					?>
				</ul>
				
                </div>
            </div>


	<?php
	include_once($path . $page); 
	?>

	<!-- Footer -->
	<footer>
	    <div class="page-container">
		<div class="uk-grid uk-grid-width-1-3">
			<div>
				<h3>Website</h3>
				<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit</p>
			</div>
			<div>
				<!-- 
				// TODO: Secure the flag
				// <?= $flags->source; ?> 
				-->
				<h3>Ressources</h3>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
			</div>
			<div>
				<h3>Contact</h3>
				<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit</p>
			</div>
		</div>
	   </div>
	</footer>
	<!-- End footer -->

	</div>
    </body>
</html>
