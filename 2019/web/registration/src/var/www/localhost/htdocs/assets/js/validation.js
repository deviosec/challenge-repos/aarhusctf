function validate_field(name, val)
{

    console.log("Named: " + name);
    console.log("Value: " + val);
    switch(name)
    {
        case 'f1-name':
            if(val.length == 0){
                alert("Please provide a name");
                return false
            }
            break;

        case 'f1-email':
            var regex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/
            if(val.length == 0){                
                alert("Please provide a email");
                return false;
            }

            if(!regex.test(val)){
                alert("The email seem to be invalid");
                return false;
            }
            break;
    }
    return true;
}