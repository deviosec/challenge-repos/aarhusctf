
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Register</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <noscript><style> .jsonly { display: none } </style></noscript>
    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1>Registration</h1>
                        <div class="description">
                       	    <p>
                                Here you can register for flags <i class="fa fa-flag"></i>. Just complete the form below.
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
                    	<form role="form" method="post" class="f1" id="f1">

                    		<h3>Personal information</h3>
                            <p class="jsonly">We store this data insecurely, so be sure to enter all your personal details.</p>
                            
                            <noscript>
                                <p>Please note you will need to have javascript enabled to complete this signup.</p>
                            </noscript>

                            <?php 
                            $is_name = isset($_POST['f1-name']);
                            $is_email = isset($_POST['f1-email']);
                            $is_captcha = isset($_POST['f1-captcha']);

                            if($is_email || $is_name){
                                if(!$is_email || !$is_name){
                                    echo "<p class='msg'>Please fill out all fields!</p>";
                                } else if($_POST['f1-captcha'] !== 'oh yes, im a human!'){
                                    echo "<p class='msg'>We do not give flags to bots!</p>";
                                } else if($_POST['f1-email'] !== 'flugel_knutz@gmail.de'){
                                    echo "<p class='msg'>Sorry, we only have one flag left. We saved it for flugel_knutz@gmail.de.</p>";
                                } else {
                                    echo "<p class='msg'>Hi flugel_knutz@gmail.de, here is your flag: #FLAG#.</p>";
                                }
                            }
                            ?>
                            <fieldset class="jsonly">
                                <div class="form-group">
                                    <label class="sr-only" for="f1-name">Name</label>
                                    <input type="text" name="f1-name" autocomplete="off" placeholder="Name..." class="f1-email form-control" id="f1-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-email">Email</label>
                                    <input type="text" name="f1-email" autocomplete="off" placeholder="Email..." class="f1-email form-control" id="f1-email">
                                </div>
                                <div class="form-group" style="display: none">
                                    <label class="sr-only" for="f1-email">Anti-bot</label>
                                    <input type="text"  name="f1-captcha" autocomplete="off" class="f1-email form-control" id="f1-captcha" value="Got cha!">
                                </div>
                                <div class="f1-buttons">
                                    <button type="submit" class="btn btn-next">Generate</button>
                                </div>
                            </fieldset>

                                                	</form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script src="assets/js/validation.js"></script>
    </body>
</html>