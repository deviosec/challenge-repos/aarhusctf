#!/usr/bin/env sh

# check that the server is alive and active
URL="http://127.0.0.1"
wget $URL -O - 
if [ $? -ne 0 ]; then 
    exit 1
fi
exit 0
