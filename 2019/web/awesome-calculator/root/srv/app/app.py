from flask import Flask, request
app = Flask(__name__)

import subprocess

header = '''
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="static/style.css">
<title>Awesome Calculator</title>
</head>
<body>
<div class="container">
<div class="content">
'''

footer = '''
</div>
</div>
</body>
</html>
'''

@app.route('/')
def root():
    html = header+'''
Welcome to my awesome calculator!<br/>
It can be used for the most basic of calculations, try it out!<br/>
<form action="/calc" method="get">
<input type="text" name="exp" placeholder="2+2"/>
<input type="submit"/>
</form>
    '''+footer
    return html


@app.route('/calc')
def make_calc():
    exp = request.args.get('exp')
    if " " in exp or "flag.txt" in exp:
        return "Hit by firewall powered by AI!"
    makeCalc = 'bc<<<"'+exp+'"'
    output = subprocess.check_output(makeCalc, shell=True, executable='/bin/sh')
    return header+'<p>Your result is: <b>'+output.decode("utf8")+'</b></p>'+footer

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')

