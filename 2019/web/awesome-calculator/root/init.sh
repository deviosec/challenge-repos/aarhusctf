#!/usr/bin/env bash

# setup our flag, and completely remove it!
if [ ! -z $FLAG ]; then
    echo $FLAG > /srv/app/flag.txt
    unset FLAG
    exec sh /init.sh
fi

# start our webserver
python3 -m flask run --host ${FLASK_HOST}
