#!/bin/sh

# check server is active
wget -O - http://127.0.0.1:5000/calc?exp=2%2B2 | grep 4
if [ $? -ne 0 ]; then
    exit 1
fi

# ensure flag is still there
ls /srv/app/flag.txt
if [ $? -ne 0 ]; then
    exit 1
fi

exit 0
