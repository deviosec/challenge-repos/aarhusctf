#!/usr/bin/env sh
URL="http://127.0.0.1/"

curl -X FLAG -v $URL 2>&1 | grep -i "200 OK"
if [ $? -ne 0 ]; then 
    exit 1
fi
exit 0
