#!/bin/sh

# plant the flag
sed -i "s/#FLAG#/$FLAG/" /var/www/localhost/htdocs/index.php
unset FLAG

# start php7-fpm in background
/usr/sbin/php-fpm7 &

# start nginx
/usr/sbin/nginx &

while sleep 10; do
    ps aux |grep nginx |grep -q -v grep
    NGINX_STATUS=$?
    ps aux |grep php-fpm7 |grep -q -v grep
    PHP_STATUS=$?

    # If the greps above find anything, they exit with 0 status
    # If they are not both 0, then something is wrong
    if [ $NGINX_STATUS -ne 0 -o $PHP_STATUS -ne 0 ]; then
      echo "Nginx or php has exited."
      exit 1
    fi
done
