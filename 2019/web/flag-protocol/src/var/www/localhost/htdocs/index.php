<html>
    <head>
        <title>Give Flag</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="container">
            <div class="content">
                <?php
                if($_SERVER['REQUEST_METHOD'] == 'FLAG'){
                    echo '#FLAG#';
                } else {
                    echo '<p>You are doing it wrong. This should be a flag!</p>';
                }
                echo "\n";
                ?>
            </div>
        </div>
    </body>
</html>