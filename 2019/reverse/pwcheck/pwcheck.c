#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main()
{
    char username[32];
    char password[32];

    setbuf(stdout, NULL);

    printf("Username: ");
    fgets(username, 32, stdin);
    printf("Password: ");
    fgets(password, 32, stdin);

    int res = strcmp(username, "secretuser\n");;
    // "vIzZT53L\n\0"
    res |= password[0] ^ 'v';
    res |= password[1] ^ 'I';
    res |= password[2] ^ 'z';
    res |= password[3] ^ 'Z';
    res |= password[4] ^ 'T';
    res |= password[5] ^ '5';
    res |= password[6] ^ '3';
    res |= password[7] ^ 'L';
    res |= password[8] ^ '\n';
    res |= password[9] ^ '\0';

    if (res == 0)
    {
        system("cat flag.txt");
        return 0;
    }
    printf("Sorry :(\n");
    return 0;
}
    
