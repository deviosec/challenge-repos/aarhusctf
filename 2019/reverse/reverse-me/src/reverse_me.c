#include <assert.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const size_t aesni_block_size = 16;
const size_t aesni_key_size_128 = 16;
const size_t aesni_round_keys_size_128 = 160;
bool aesni_available();
void aesni_key_expansion_128(void* round_keys, const void* key);
void aesni_encrypt_block_128(void* ciphertext, const void* plaintext, const void* round_keys);

void keygen(void* buffer)
{
    srand(42);
    unsigned char* key = buffer;
    for (size_t i = 0; i < aesni_key_size_128; ++i)
    {
        key[i] = (rand() & 0xff) ^ 47;
    }
}

size_t read_flag(char* buffer, size_t length)
{
    int f = open("flag.txt", O_RDONLY);
    if (f == -1)
    {
        perror("open");
        exit(1);
    }
    size_t data_read = 0;
    while (true)
    {
        ssize_t ret = read(f, buffer + data_read, length - data_read);
        if (ret == -1)
        {
            perror("read");
            exit(1);
        }
        else if (ret == 0)
        {
            break;
        }
        data_read += ret;
    }
    int ret = close(f);
    if (ret == -1)
    {
        perror("close");
        exit(1);
    }
    return data_read;
}

void pad(char* buffer, size_t data_length, size_t padded_length)
{
    assert(padded_length > data_length && (padded_length - data_length) < 256);
    size_t padding_length = padded_length - data_length;
    unsigned char pad_byte = padding_length & 0xff;
    memset(buffer + data_length, pad_byte, padding_length);
}


void xor_block(uint8_t* xs, const uint8_t* ys)
{
    for (size_t i = 0; i < 16; ++i)
        xs[i] ^= ys[i];
}

void encrypt(const void* key, uint8_t* ciphertext, const void* plaintext, size_t length)
{
    /* generate IV */
    srand(47);
    for (size_t i = 0; i < aesni_block_size; ++i)
    {
        ciphertext[i] = (rand() & 0xff) ^ 42;
    }
    memcpy(ciphertext + aesni_block_size, plaintext, length - aesni_block_size);

    uint8_t round_keys[aesni_round_keys_size_128];
    aesni_key_expansion_128(round_keys, key);

    size_t num_blocks = length / aesni_block_size;
    for (size_t i = 1; i < num_blocks; ++i)
    {
        xor_block(ciphertext + i*aesni_block_size, ciphertext + (i-1)*aesni_block_size);
        aesni_encrypt_block_128(ciphertext + i*aesni_block_size, ciphertext + i*aesni_block_size, round_keys);
    }

}

void write_ciphertext(uint8_t* buffer, size_t length)
{
    int f = open("flag.enc", O_RDWR | O_CREAT, 0644);
    if (f == -1)
    {
        perror("open");
        exit(1);
    }
    size_t data_written = 0;
    while (data_written < length)
    {
        ssize_t ret = write(f, buffer + data_written, length - data_written);
        if (ret == -1)
        {
            perror("write");
            exit(1);
        }
        data_written += ret;
    }
    int ret = close(f);
    if (ret == -1)
    {
        perror("close");
        exit(1);
    }
}




int main()
{
    if (!aesni_available())
    {
        exit(1);
    }

    char flag[48];
    size_t buf_length = sizeof(flag);
    size_t flag_length = read_flag(flag, buf_length - 1);
    pad(flag, flag_length, buf_length);

    uint8_t key[aesni_key_size_128];
    keygen(key);

    uint8_t ciphertext[64];
    encrypt(key, ciphertext, flag, 64);
    write_ciphertext(ciphertext, 64);
}
