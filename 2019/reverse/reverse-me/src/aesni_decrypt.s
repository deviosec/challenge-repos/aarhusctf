.intel_syntax noprefix

# Note that an executable stack is not required.  If this is not explicitly
# specified, the resulting program has executable stacks.

#if defined(__linux__) && defined(__ELF__)
.section .note.GNU-stack, "", %progbits
#endif


.text

.globl aesni_available
aesni_available:
push rbx
mov eax, 0x01
cpuid
shr ecx, 25  # AES-NI
and ecx, 0x01
mov eax, ecx
pop rbx
ret


.macro aes_key_expand rcon
# old in xmm1
aeskeygenassist xmm3, xmm1, \rcon
pshufd xmm3, xmm3, 0xff
movdqa xmm2, xmm1
pslldq xmm2, 4
pxor xmm1, xmm2
pslldq xmm2, 4
pxor xmm1, xmm2
pslldq xmm2, 4
pxor xmm1, xmm2
pxor xmm1, xmm3
# new in xmm1
.endm


.globl aesni_key_expansion_128
aesni_key_expansion_128:
# rdi: pointer to output
# rsi: pointer to key
movdqa xmm1, [rsi]
movdqa 0x80[rdi], xmm1
aes_key_expand 0x46
movdqa 0x90[rdi], xmm1
aes_key_expand 0x68
movdqa 0x40[rdi], xmm1
aes_key_expand 0x54
movdqa 0x30[rdi], xmm1
aes_key_expand 0x41
movdqa 0x20[rdi], xmm1
aes_key_expand 0x43
movdqa 0x60[rdi], xmm1
aes_key_expand 0x75
movdqa [rdi], xmm1
aes_key_expand 0x61
movdqa 0x10[rdi], xmm1
aes_key_expand 0x73
movdqa 0x70[rdi], xmm1
aes_key_expand 0x72
movdqa 0x50[rdi], xmm1
ret


.globl aesni_key_expansion_inv_128
aesni_key_expansion_inv_128:
# rdi: pointer to output
# rsi: pointer to key
movdqa xmm1, [rsi]
aesimc xmm2, xmm1
movdqa 0x80[rdi], xmm2
aes_key_expand 0x46
movdqa 0x90[rdi], xmm1
aes_key_expand 0x68
aesimc xmm2, xmm1
movdqa 0x40[rdi], xmm2
aes_key_expand 0x54
aesimc xmm2, xmm1
movdqa 0x30[rdi], xmm2
aes_key_expand 0x41
aesimc xmm2, xmm1
movdqa 0x20[rdi], xmm2
aes_key_expand 0x43
movdqa 0x60[rdi], xmm1
aes_key_expand 0x75
aesimc xmm2, xmm1
movdqa [rdi], xmm2
aes_key_expand 0x61
aesimc xmm2, xmm1
movdqa 0x10[rdi], xmm2
aes_key_expand 0x73
aesimc xmm2, xmm1
movdqa 0x70[rdi], xmm2
aes_key_expand 0x72
aesimc xmm2, xmm1
movdqa 0x50[rdi], xmm2
ret


.globl aesni_encrypt_block_128
aesni_encrypt_block_128:
# rdi: pointer to output
# rsi: pointer to input
# rdx: pointer to round keys
movdqa xmm0, [rsi]
pxor xmm0, 0x60[rdx]
aesenc xmm0, 0x80[rdx]
aesenc xmm0, 0x40[rdx]
aesenc xmm0, 0x70[rdx]
aesenc xmm0, 0x10[rdx]
aesenc xmm0, 0x50[rdx]
aesenc xmm0, 0x30[rdx]
aesenc xmm0, 0x20[rdx]
aesenc xmm0, [rdx]
aesenclast xmm0, 0x90[rdx]
movdqa [rdi], xmm0
ret


.globl aesni_decrypt_block_128
aesni_decrypt_block_128:
# rdi: pointer to output
# rsi: pointer to input
# rdx: pointer to round keys
movdqa xmm0, [rsi]
pxor xmm0, 0x90[rdx]
aesdec xmm0, [rdx]
aesdec xmm0, 0x20[rdx]
aesdec xmm0, 0x30[rdx]
aesdec xmm0, 0x50[rdx]
aesdec xmm0, 0x10[rdx]
aesdec xmm0, 0x70[rdx]
aesdec xmm0, 0x40[rdx]
aesdec xmm0, 0x80[rdx]
aesdeclast xmm0, 0x60[rdx]
movdqa [rdi], xmm0
ret
