#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

const size_t aesni_block_size = 16;
const size_t aesni_key_size_128 = 16;
const size_t aesni_round_keys_size_128 = 160;
bool aesni_available();
void aesni_key_expansion_inv_128(void* round_keys, const void* key);
void aesni_decrypt_block_128(void* ciphertext, const void* plaintext, const void* round_keys);

void keygen(void* buffer)
{
    srand(42);
    unsigned char* key = buffer;
    for (size_t i = 0; i < aesni_key_size_128; ++i)
    {
        key[i] = (rand() & 0xff) ^ 47;
    }
}

size_t read_ciphertext(uint8_t* buffer, size_t length)
{
    int f = open("flag.enc", O_RDONLY);
    if (f == -1)
    {
        perror("open");
        exit(1);
    }
    size_t data_read = 0;
    while (true)
    {
        ssize_t ret = read(f, buffer + data_read, length - data_read);
        if (ret == -1)
        {
            perror("read");
            exit(1);
        }
        else if (ret == 0)
        {
            break;
        }
        data_read += ret;
    }
    int ret = close(f);
    if (ret == -1)
    {
        perror("close");
        exit(1);
    }
    return data_read;
}


void xor_block(uint8_t* xs, const uint8_t* ys)
{
    for (size_t i = 0; i < 16; ++i)
        xs[i] ^= ys[i];
}

void decrypt(const void* key, uint8_t* plaintext, const uint8_t* ciphertext, size_t length)
{
    /* memcpy(ciphertext + aesni_block_size, plaintext, length - aesni_block_size); */

    uint8_t round_keys[aesni_round_keys_size_128];
    aesni_key_expansion_inv_128(round_keys, key);

    size_t num_blocks = length / aesni_block_size;
    for (size_t i = 0; i < num_blocks - 1; ++i)
    {
        aesni_decrypt_block_128(plaintext + i*aesni_block_size, ciphertext + (i+1)*aesni_block_size, round_keys);
        xor_block(plaintext + i*aesni_block_size, ciphertext + i*aesni_block_size);
    }

}

int main()
{
    uint8_t key[aesni_key_size_128];
    uint8_t ciphertext[4*aesni_block_size];
    uint8_t plaintext[3*aesni_block_size];
    keygen(key);
    read_ciphertext(ciphertext, 4*aesni_block_size);
    decrypt(key, plaintext, ciphertext, 4*aesni_block_size);
    write(1, plaintext, 39);
}
