function verify_flag(flag) {
	const a = 't0n{FTC';
	const b = '}r4h';
	const c = '0ot';
	
	

	const parts = flag.split('_');
	if (parts.length !== 3 ||
		parts[0].split('').reverse().join('') !== a ||
		parts[1].split('').reverse().join('') !== c ||
		parts[2].split('').reverse().join('') !== b) {
		return false;
	}
	
	return true;
}
