===Nicolas===
Title: The Luxurious Program!
Description: Treat me well <3<3<3 - and I will reward you with the secret flag!
Flag: CTF{oohh_thank_you_very_much_!!!_<3_<3_<3}
Hint: The program expects luxury treats in its environment and on its command-line.
Difficulty: Hard
Files:
	- crackme0x0
