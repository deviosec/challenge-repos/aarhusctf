#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const size_t num_books = 8;

typedef struct
{
    bool active;
    size_t length;
    char author[64];
    char title[64];
    char* content;
} book_t;

size_t get_index()
{
    char buf[10];
    fgets(buf, 10, stdin);
    return strtoul(buf, NULL, 0);
}

void list_books(const book_t* books, size_t n, FILE* file)
{
    for (size_t i = 0; i < n; ++i)
    {
        if (books[i].active)
            fprintf(file, "%zu) %s -- %s\n", i, books[i].author, books[i].title);
        else
            fprintf(file, "%zu)\n", i);
    }
}

void display_book(const book_t* book, FILE* file)
{
    assert(book->active);
    fprintf(file, "============================================================\n");
    fprintf(file, "%s -- %s\n\n", book->author, book->title);
    fprintf(file, book->content);
    fprintf(file, "\n\nThe End.\n\n");
    fprintf(file, "============================================================\n");
}

void add_book(book_t* book)
{
    printf("Enter author\n> ");
    fgets(book->author, sizeof(book->author), stdin);
    char* n = strchr(book->author, '\n');
    if (n != NULL)
        *n = '\0';
    printf("Enter title\n> ");
    fgets(book->title, sizeof(book->author), stdin);
    n = strchr(book->title, '\n');
    if (n != NULL)
        *n = '\0';
    printf("Enter content length\n> ");
    book->length = get_index();
    book->content = malloc(book->length + 1);
    printf("Enter content\n> ");
    fgets(book->content, book->length + 1, stdin);
    book->active = true;
}

void del_book(book_t* book)
{
    if (book->active)
    {
        free(book->content);
        book->active = false;
    }
}

size_t book_select()
{
    printf("Which position?\n> ");
    size_t index = get_index();
    return index;
}

int main()
{
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);

    book_t books[num_books];
    memset(books, 0x00, sizeof(books));
    printf("Welcome to the Bookmanager 4.7\n\n");

    while (true)
    {
        printf("What would you like to do?\n");
        printf("0) list books\n"
               "1) read book\n"
               "2) add book\n"
               "3) delete book\n"
               "4) exit\n> ");
        size_t option = get_index();
        switch (option)
        {
        case 0:
            list_books(books, num_books, stdout);
            break;
        case 1:
            {
                size_t index = book_select();
                if (books[index % num_books].active)
                    display_book(&books[index % num_books], stdout);
                else
                    printf("There is no book at position %zu.\n", index % num_books);
            }
            break;
        case 2:
            {
                size_t index = book_select();
                if (books[index % num_books].active)
                    printf("There is already a book at position %zu.\n", index % num_books);
                else
                    add_book(&books[index % num_books]);
            }
            break;
        case 3:
            {
                size_t index = book_select();
                del_book(&books[index % num_books]);
            }
            break;
        case 4:
        default:
            for (size_t i = 0; i < num_books; ++i)
                del_book(&books[i]);
            printf("Goodbye!\n");
            return 0;
        }
    }
}
