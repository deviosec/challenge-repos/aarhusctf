#!/bin/sh
# setup our flag, and completely remove it!
if [ ! -z $FLAG ]; then
    echo $FLAG > /home/user/flag.txt
    unset FLAG
    exec sh /home/user/init.sh
fi

# start socat
socat TCP-LISTEN:4747,reuseaddr,fork EXEC:"./bookmanager"
