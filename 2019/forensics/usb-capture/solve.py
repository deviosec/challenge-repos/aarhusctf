#!/usr/bin/env python3

# note that this solution is a bit hacky. It uses US localization with manual
# changes to make the result look like a flag. The only thing it's missing, is
# the appostrophe, which is one of the nulls (but cbb to fix it).

from scapy.all import *

pkgs = rdpcap('usb.pcapng')

lkeys = b'\x00\x00\x00\x00abcdefghijklmnopqrstuvwxyz123456{89}\x00\x00\x00\x00 -=[]\\\x00;\'\x00,./'
ukeys = b'\x00\x00\x00\x00ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()\x00\x00\x00\x00 \x00+{}|~:"~<>_'

flag = ''

for pkg in pkgs:
    data = pkg.load
    if data[8] == 0x43:  # complete
        leftovers = data[-8:]
        keys = leftovers[-7:]
        for k in keys:
            if leftovers[0] == 0x02:
                x = ukeys[k]
            else:
                x = lkeys[k]
            if x != 0:
                flag += chr(x)

print(f'flag = {flag}')
