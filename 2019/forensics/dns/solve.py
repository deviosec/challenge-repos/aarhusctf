#!/usr/bin/env python3

from scapy.all import *
from base64 import urlsafe_b64decode as b64d

print('[*] loading junk.pcap')
pkgs = rdpcap('junk.pcap')

zf = list()
kb = list()
for p in pkgs:
    if IP in p and p[IP].dst == '192.168.87.1':
        zf.append(p[DNS].qd.qname)
        kb.append(p[UDP].dport)

z = b''.join(z.decode('ascii').split('.')[1].encode('ascii') for z in zf)
k = bytes(kb)

print(f'[*] key = {k}')
print(f'[*] writing file')

with open('output.zip', 'wb') as f:
    f.write(b64d(z))
